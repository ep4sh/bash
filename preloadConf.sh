#!/bin/bash
##   preload && compose-cache installer
###  by ep4sh2k@gmail.com

sudo apt-get install preload -y
sudo touch /var/lib/preload/preload.state
sudo chmod 600 /var/lib/preload/preload.state
sudo /etc/init.d/preload restart
if [[ -n `sudo cat /var/lib/preload/preload.state` ]]
  then
    echo "PRELOAD SUCCESSFULL" 
  else
    echo "PRELOAD FAIL"
fi

cd /home/user
mkdir ~/.compose-cache
sudo mkdir /home/user/.compose-cache 
sudo chown user:user /home/user/.compose-cache
