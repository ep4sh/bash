#!/bin/sh
#byep4sh2k@gmail.com
start=`date +%Y-%m-%d---%H-%M`
trash="/volume1/users/logistic/****/.trash"
end=`date +%Y-%m-%d---%H-%M`

echo "@$start..Start delete from .trash files" >> /var/log/synologyClear.log

find "${trash}" -type f  -mtime +96 -print0    | xargs -0 -I {} rm -rf {}


end=`date +%Y-%m-%d---%H-%M`
echo "@$end..Finish moving to .trash  files..Job done!" >> /var/log/synologyClear.log


