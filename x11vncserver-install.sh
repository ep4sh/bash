sudo apt-get install x11vnc
sudo x11vnc -storepasswd /etc/x11vnc.pass
sudo x11vnc -display :0 -rfbauth /etc/x11vnc.pass -rfbport 5900 -o /var/log/x11vnc.log

sudo tee <<EOF >  /lib/systemd/system/x11vnc.service
[Unit]
Description=VNC remote access
Requires=display-manager.service
After=display-manager.service

[Service]
ExecStart=/usr/bin/x11vnc -xkb -auth guess -noxrecord -noxfixes -shared -noxdamage -rfbauth /etc/x11vnc.pass -forever -loop -rfbport 5900 -o /var/log/x11vnc.log
ExecStop=/usr/bin/killall x11vnc

[Install]
WantedBy=multi-user.target
EOF


sudo systemctl start x11vnc.service 
sudo systemctl enable x11vnc.service 
sudo systemctl status x11vnc.service 

