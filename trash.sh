#!/bin/sh
#byep4sh2k@gmail.com
start=`date +%Y-%m-%d---%H-%M`
trash="/volume1/users/logistic/****/.trash"
notDeletePath="/volume1/users/logistic/****/main/*"

echo "@$start..Start moving to .trash files" #>> /var/log/synologyClear.log

if [ ! -d "$trash" ]
then
  mkdir "${trash}"
  echo "Creating .trash dir"
fi

find /home/ep4sh/test -path "${notDeletePath}" -prune -o -type f  -mtime +93 -print0    | xargs -0 -I {} mv {} "${trash}"

end=`date +%Y-%m-%d---%H-%M`
echo "@$end..Finish moving to .trash  files..Job done!" #>> /var/log/synologyClear.log
