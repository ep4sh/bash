#!/bin/sh
# Скрипт для синхронизации писем между двумя почтовыми серверами
#
#Configure User
SERVER1=$1
UNAME1=$2
PWORD1=$3
SERVER2=$4
UNAME2=$5
PWORD2=$6
#Blank this out if you want to see folder sizes
HIDE="--nofoldersizes --skipsize"
/usr/bin/imapsync --syncinternaldates --useheader 'Message-Id' \
--host1 ${SERVER1} --user1 ${UNAME1} \
--password1 ${PWORD1} --ssl1 \
--host2 ${SERVER2} \
--port2 993 --user2 ${UNAME2} \
--password2 ${PWORD2} --ssl2 \
--authmech1 LOGIN --authmech2 LOGIN --split1 200 --split2 200 ${HIDE} \
#--exclude 'Drafts|Trash|Spam|Sent'


#TO Sync Special Folders to Gmail
#imapsync --syncinternaldates --useheader 'Message-Id' \
#--host1 ${SERVER1} --user1 ${UNAME1} \
#--password1 ${PWORD1} --ssl1 \
#--host2 imap.googlemail.com \
#--port2 993 --user2 ${UNAME2} \
#--password2 ${PWORD2} --ssl2 \
#--ssl2 --noauthmd5 --split1 200 --split2 200 ${HIDE} \
#--folder "Inbox/Sent" --prefix2 '[Gmail]/' --regextrans2 's/Inbox\/Sent/Sent Mail/' \
#--folder "Inbox/Spam" --prefix2 '[Gmail]/' --regextrans2 's/Inbox\/Spam/Spam/' \
#--folder "Inbox/Trash" --prefix2 '[Gmail]/' --regextrans2 's/Inbox\/Trash/Trash/' \
#--folder "Inbox/Drafts" --prefix2 '[Gmail]/' --regextrans2 's/Inbox\/Drafts/Drafts/' \

