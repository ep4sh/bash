echo<<EOF > /etc/netplan/01-network-manager-all.yaml 

# Let NetworkManager manage all devices on this system
network:
  version: 2
  renderer: networkd
  ethernets:
          enp4s0:
                  dhcp4: no
                  addresses: [192.168.1.162/24]
                  gateway4: 192.168.1.1
                  nameservers:
                          addresses: [192.168.1.1,8.8.8.8]