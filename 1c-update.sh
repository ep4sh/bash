#!/bin/bash
#by ep4sh2k@gmail.com

#Скрипт для обновления 1с
#Перед обновлением нужно скачать и положить на 192.168.1.79/users/logistic/1c-update
#следующие .deb-пакеты новой версии: 
#1c-enterprise83-server_8.3.10-2753_i386
#1c-enterprise83-common_8.3.10-2753_i386
#1c-enterprise83-client_8.3.10-2753_i386


UPDATE_DIR="/mnt/arc/1c-update"
cd ${UPDATE_DIR}

if [[ ! -d $UPDATE_DIR ]]; then
  echo "/mnt/arc is not mounted - choose another update directory!"
  exit
fi

if [[ ! -r $UPDATE_DIR ]]; then
  echo "Can't read data from $UPDATE_DIR - check permissions"
fi
sudo dpkg -i 1c-enterprise83-serv*
sudo dpkg -i 1c-enterprise83-common*
sudo dpkg -i 1c-enterprise83-client*
sudo apt install -y libgsf-bin:i386

